﻿using CodeItAirlines.Exceptions;
using CodeItAirlines.Locais;
using CodeItAirlines.Locais.Interface;
using CodeItAirlines.Pessoas;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using CodeItAirlines.Validacao.Interface;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CodeItAirlines
{
    public partial class Main : Form
    {
        private ILugar _aviao;
        private ILugar _terminal;
        private SmartFortwo _veiculo;
        private bool _desembarque = false;

        private IValidacaoMotorista _validacaoMotorista;

        public Main()
        {
            InitializeComponent();
            InitializeData();
            InitializeForm();
        }

        private void InitializeData()
        {
            _validacaoMotorista = new ValidacaoSmartFortwo();

            _aviao = new Aviao(new List<IPessoa>());

            _terminal = new Terminal(new List<IPessoa>
            {
                new ChefeServicoVoo(),
                new Comissaria(),
                new Comissaria(),
                new Oficial(),
                new Oficial(),
                new Piloto(),
                new Policial(),
                new Presidiario()
            });

            _veiculo = new SmartFortwo();
        }

        private void InitializeForm()
        {
            foreach (var pessoa in _terminal.Pessoas)
                lbxTerminal.Items.Add(pessoa.Nome);
        }

        private void btnAcao_Click(object sender, EventArgs e)
        {
            _desembarque = !_desembarque;

            btnAcao.Text = _desembarque
                         ? "Embarque"
                         : "Desembarque";

            gnOcupantes.Text = _desembarque
                             ? "Desembarcar"
                             : "Embarcar";

            gnOcupantes.RightToLeft = _desembarque
                                    ? RightToLeft.Yes
                                    : RightToLeft.No;
        }

        private void btnMotorista_Click(object sender, EventArgs e)
        {
            mudarPessoaLugar(_desembarque ? _aviao : _terminal,
                             _desembarque ? lbxAviao : lbxTerminal,
                             txtMotorista,
                             true);
        }

        private void btnPassageiro_Click(object sender, EventArgs e)
        {
            mudarPessoaLugar(_desembarque ? _aviao : _terminal,
                             _desembarque ? lbxAviao : lbxTerminal,
                             txtPassageiro,
                             false);
        }

        private void mudarPessoaLugar(ILugar lugar, ListBox lista, TextBox assento, bool ehMotorista)
        {
            var indice = lista.SelectedIndex;
            if (indice.Equals(-1) && string.IsNullOrWhiteSpace(assento.Text))
                return;

            try
            {
                if (!string.IsNullOrWhiteSpace(assento.Text))
                {
                    var pessoa = ehMotorista ? _veiculo.Motorista : _veiculo.Passageiro;

                    lugar.AdicionarPessoa(pessoa);
                    lista.Items.Add(pessoa.Nome);
                    assento.Text = string.Empty;

                    if (ehMotorista)
                        _veiculo.Motorista = null;
                    else
                        _veiculo.Passageiro = null;
                }

                if (!indice.Equals(-1))
                {
                    var pessoa = lugar.RemoverPessoa(indice);

                    assento.Text = pessoa.Nome;
                    lista.Items.RemoveAt(indice);

                    if (ehMotorista)
                        _veiculo.Motorista = pessoa as IMotorista;
                    else
                        _veiculo.Passageiro = pessoa;

                    _validacaoMotorista.Validar(_veiculo);
                }
            }
            catch (ValidacaoException e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}