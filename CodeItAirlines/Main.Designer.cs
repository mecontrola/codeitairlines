﻿namespace CodeItAirlines
{
    partial class Main
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbPassageiro = new System.Windows.Forms.Label();
            this.lbMotorista = new System.Windows.Forms.Label();
            this.gnOcupantes = new System.Windows.Forms.GroupBox();
            this.btnMotorista = new System.Windows.Forms.Button();
            this.btnPassageiro = new System.Windows.Forms.Button();
            this.btnAcao = new System.Windows.Forms.Button();
            this.txtMotorista = new System.Windows.Forms.TextBox();
            this.txtPassageiro = new System.Windows.Forms.TextBox();
            this.lbxAviao = new System.Windows.Forms.ListBox();
            this.lbxTerminal = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gnOcupantes.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lbxAviao);
            this.panel1.Controls.Add(this.lbxTerminal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lbPassageiro);
            this.panel2.Controls.Add(this.lbMotorista);
            this.panel2.Controls.Add(this.gnOcupantes);
            this.panel2.Controls.Add(this.btnAcao);
            this.panel2.Controls.Add(this.txtMotorista);
            this.panel2.Controls.Add(this.txtPassageiro);
            this.panel2.Location = new System.Drawing.Point(224, 88);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(352, 350);
            this.panel2.TabIndex = 2;
            // 
            // lbPassageiro
            // 
            this.lbPassageiro.AutoSize = true;
            this.lbPassageiro.Location = new System.Drawing.Point(9, 175);
            this.lbPassageiro.Name = "lbPassageiro";
            this.lbPassageiro.Size = new System.Drawing.Size(59, 13);
            this.lbPassageiro.TabIndex = 7;
            this.lbPassageiro.Text = "Passageiro";
            // 
            // lbMotorista
            // 
            this.lbMotorista.AutoSize = true;
            this.lbMotorista.Location = new System.Drawing.Point(4, 101);
            this.lbMotorista.Name = "lbMotorista";
            this.lbMotorista.Size = new System.Drawing.Size(50, 13);
            this.lbMotorista.TabIndex = 6;
            this.lbMotorista.Text = "Motorista";
            // 
            // gnOcupantes
            // 
            this.gnOcupantes.Controls.Add(this.btnMotorista);
            this.gnOcupantes.Controls.Add(this.btnPassageiro);
            this.gnOcupantes.Location = new System.Drawing.Point(3, 281);
            this.gnOcupantes.Name = "gnOcupantes";
            this.gnOcupantes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gnOcupantes.Size = new System.Drawing.Size(346, 51);
            this.gnOcupantes.TabIndex = 5;
            this.gnOcupantes.TabStop = false;
            this.gnOcupantes.Text = "Embarcar";
            // 
            // btnMotorista
            // 
            this.btnMotorista.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMotorista.Location = new System.Drawing.Point(6, 19);
            this.btnMotorista.Name = "btnMotorista";
            this.btnMotorista.Size = new System.Drawing.Size(164, 23);
            this.btnMotorista.TabIndex = 3;
            this.btnMotorista.Text = "Motorista";
            this.btnMotorista.UseVisualStyleBackColor = true;
            this.btnMotorista.Click += new System.EventHandler(this.btnMotorista_Click);
            // 
            // btnPassageiro
            // 
            this.btnPassageiro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPassageiro.Location = new System.Drawing.Point(173, 19);
            this.btnPassageiro.Name = "btnPassageiro";
            this.btnPassageiro.Size = new System.Drawing.Size(164, 23);
            this.btnPassageiro.TabIndex = 4;
            this.btnPassageiro.Text = "Passageiro";
            this.btnPassageiro.UseVisualStyleBackColor = true;
            this.btnPassageiro.Click += new System.EventHandler(this.btnPassageiro_Click);
            // 
            // btnAcao
            // 
            this.btnAcao.Location = new System.Drawing.Point(3, 252);
            this.btnAcao.Name = "btnAcao";
            this.btnAcao.Size = new System.Drawing.Size(346, 23);
            this.btnAcao.TabIndex = 2;
            this.btnAcao.Text = "Desembarcar";
            this.btnAcao.UseVisualStyleBackColor = true;
            this.btnAcao.Click += new System.EventHandler(this.btnAcao_Click);
            // 
            // txtMotorista
            // 
            this.txtMotorista.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMotorista.Location = new System.Drawing.Point(3, 120);
            this.txtMotorista.Name = "txtMotorista";
            this.txtMotorista.Size = new System.Drawing.Size(346, 20);
            this.txtMotorista.TabIndex = 1;
            // 
            // txtPassageiro
            // 
            this.txtPassageiro.Location = new System.Drawing.Point(3, 194);
            this.txtPassageiro.Name = "txtPassageiro";
            this.txtPassageiro.Size = new System.Drawing.Size(346, 20);
            this.txtPassageiro.TabIndex = 0;
            // 
            // lbxAviao
            // 
            this.lbxAviao.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxAviao.Location = new System.Drawing.Point(582, 88);
            this.lbxAviao.Name = "lbxAviao";
            this.lbxAviao.Size = new System.Drawing.Size(206, 342);
            this.lbxAviao.TabIndex = 4;
            // 
            // lbxTerminal
            // 
            this.lbxTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxTerminal.Location = new System.Drawing.Point(12, 88);
            this.lbxTerminal.Name = "lbxTerminal";
            this.lbxTerminal.Size = new System.Drawing.Size(206, 342);
            this.lbxTerminal.TabIndex = 0;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CodeItAirlines";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gnOcupantes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtMotorista;
        private System.Windows.Forms.TextBox txtPassageiro;
        private System.Windows.Forms.ListBox lbxAviao;
        private System.Windows.Forms.ListBox lbxTerminal;
        private System.Windows.Forms.Button btnMotorista;
        private System.Windows.Forms.Button btnAcao;
        private System.Windows.Forms.Button btnPassageiro;
        private System.Windows.Forms.GroupBox gnOcupantes;
        private System.Windows.Forms.Label lbPassageiro;
        private System.Windows.Forms.Label lbMotorista;
    }
}

