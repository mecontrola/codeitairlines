﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;

namespace CodeItAirlines.Pessoas
{
    public class Oficial : Pessoa
    {
        public Oficial() : base(TipoTripulacao.Tecnica, TipoPessoa.Oficial) { }
    }
}