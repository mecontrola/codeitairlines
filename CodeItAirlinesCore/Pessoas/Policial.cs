﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;
using CodeItAirlines.Pessoas.Interface;

namespace CodeItAirlines.Pessoas
{
    public class Policial : Pessoa, IMotorista
    {
        public Policial() : base(TipoTripulacao.Passageiro, TipoPessoa.Policial) { }
    }
}