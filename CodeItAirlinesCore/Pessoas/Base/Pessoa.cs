﻿using CodeItAirlines.Enum;
using CodeItAirlines.Helper;
using CodeItAirlines.Pessoas.Interface;

namespace CodeItAirlines.Pessoas.Base
{
    public abstract class Pessoa : IPessoa
    {
        public TipoTripulacao TipoTripulacao { get; private set; }
        public TipoPessoa TipoPessoa { get; private set; }

        protected Pessoa(TipoTripulacao tipoTripulacao,
                         TipoPessoa tipoPessoa)
        {
            TipoTripulacao = tipoTripulacao;
            TipoPessoa = tipoPessoa;
        }

        public string Nome => Enums.GetDescription(TipoPessoa);
    }
}