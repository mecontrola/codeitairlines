﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;

namespace CodeItAirlines.Pessoas
{
    public class Presidiario : Pessoa
    {
        public Presidiario() : base(TipoTripulacao.Passageiro, TipoPessoa.Presidiario) { }
    }
}