﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;

namespace CodeItAirlines.Pessoas
{
    public class Comissaria : Pessoa
    {
        public Comissaria() : base(TipoTripulacao.Cabine, TipoPessoa.Comissaria) { }
    }
}