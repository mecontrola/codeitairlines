﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;
using CodeItAirlines.Pessoas.Interface;

namespace CodeItAirlines.Pessoas
{
    public class ChefeServicoVoo : Pessoa, IMotorista
    {
        public ChefeServicoVoo() : base(TipoTripulacao.Cabine, TipoPessoa.ChefeServicoVoo) { }
    }
}