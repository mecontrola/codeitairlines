﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;
using CodeItAirlines.Pessoas.Interface;

namespace CodeItAirlines.Pessoas
{
    public class Piloto : Pessoa, IMotorista
    {
        public Piloto() : base(TipoTripulacao.Tecnica, TipoPessoa.Piloto) { }
    }
}