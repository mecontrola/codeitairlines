﻿using System;
using System.Runtime.Serialization;

namespace CodeItAirlines.Exceptions
{
    [Serializable()]
    public class ValidacaoException : Exception
    {
        public ValidacaoException() : base() { }
        public ValidacaoException(string mensagem) : base(mensagem) { }
        public ValidacaoException(string message, Exception inner) : base(message, inner) { }

        protected ValidacaoException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}