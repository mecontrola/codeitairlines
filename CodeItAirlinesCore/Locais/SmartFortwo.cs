﻿using CodeItAirlines.Pessoas.Interface;

namespace CodeItAirlines.Locais
{
    public class SmartFortwo
    {
        public IMotorista Motorista { get; set; }
        public IPessoa Passageiro { get; set; }
    }
}