﻿using CodeItAirlines.Enum;
using CodeItAirlines.Locais.Base;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using System.Collections.Generic;

namespace CodeItAirlines.Locais
{
    public class Aviao : Lugar
    {
        public Aviao(IList<IPessoa> pessoas)
            : base(TipoLocal.Aviao, pessoas, new ValidacaoLugar()) { }
    }
}