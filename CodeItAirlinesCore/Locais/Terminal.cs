﻿using CodeItAirlines.Enum;
using CodeItAirlines.Locais.Base;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using System.Collections.Generic;

namespace CodeItAirlines.Locais
{
    public class Terminal : Lugar
    {
        public Terminal(IList<IPessoa> pessoas)
            : base(TipoLocal.Terminal, pessoas, new ValidacaoLugar()) { }
    }
}