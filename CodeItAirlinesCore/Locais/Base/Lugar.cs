﻿using CodeItAirlines.Enum;
using CodeItAirlines.Helper;
using CodeItAirlines.Locais.Interface;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao.Interface;
using System.Collections.Generic;
using System.Linq;

namespace CodeItAirlines.Locais.Base
{
    public abstract class Lugar : ILugar
    {
        private readonly IValidacao _validacao;

        public string Nome { get; private set; }
        public IList<IPessoa> Pessoas { get; private set; }

        protected Lugar(TipoLocal tipoLocal,
                        IList<IPessoa> pessoas,
                        IValidacao validacaoLugar)
        {
            Nome = Enums.GetDescription(tipoLocal);
            Pessoas = pessoas;

            _validacao = validacaoLugar;
        }

        public void AdicionarPessoa(IPessoa pessoa)
        {
            Pessoas.Add(pessoa);

            _validacao.Validar(Pessoas);
        }

        public IPessoa RemoverPessoa(string nome)
        {
            var indice = Pessoas.IndexOf(Pessoas.Single(i => i.Nome.Equals(nome)));

            return RemoverPessoa(indice);
        }

        public IPessoa RemoverPessoa(int indice)
        {
            var pessoas = (Pessoas as List<IPessoa>)[indice];

            return RemoverPessoa(pessoas);
        }

        public IPessoa RemoverPessoa(IPessoa pessoa)
        {
            Pessoas.Remove(pessoa);

            _validacao.Validar(Pessoas);

            return pessoa;
        }
    }
}