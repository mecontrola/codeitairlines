﻿using CodeItAirlines.Pessoas.Interface;
using System.Collections.Generic;

namespace CodeItAirlines.Locais.Interface
{
    public interface ILugar
    {
        string Nome { get; }
        IList<IPessoa> Pessoas { get; }
        void AdicionarPessoa(IPessoa pessoa);
        IPessoa RemoverPessoa(int indice);
        IPessoa RemoverPessoa(string nome);
        IPessoa RemoverPessoa(IPessoa pessoa);
    }
}