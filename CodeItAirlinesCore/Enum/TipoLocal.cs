﻿using System.ComponentModel;

namespace CodeItAirlines.Enum
{
    public enum TipoLocal
    {
        [Description("Avião")]
        Aviao,
        Terminal
    }
}