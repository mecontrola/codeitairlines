﻿using System.ComponentModel;

namespace CodeItAirlines.Enum
{
    public enum TipoPessoa
    {
        Piloto,
        Oficial,
        [Description("Chefe de Serviço de Vôo")]
        ChefeServicoVoo,
        [Description("Comissária")]
        Comissaria,
        Policial,
        [Description("Presidiário")]
        Presidiario
    }
}