﻿using System.ComponentModel;

namespace CodeItAirlines.Enum
{
    public enum TipoTripulacao
    {
        [Description("Tripulação Técnica")]
        Tecnica,
        [Description("Tripulação de Cabine")]
        Cabine,
        Passageiro
    }
}