﻿using CodeItAirlines.Enum;
using CodeItAirlines.Exceptions;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao.Interface;
using System.Collections.Generic;
using System.Linq;

namespace CodeItAirlines.Validacao
{
    public class ValidacaoPresidiarioSemPolicial : IValidacao
    {
        private const string MENSAGEM_ERRO_PRESIDIARIO_SOZINHO = "Não é permitido o presidiário ficar sem a presença de um policial.";
        private const int QUANTIDADE_PRESENTE_LUGAR = 1;

        public void Validar(ICollection<IPessoa> listaPessoas)
        {
            if (listaPessoas == null || !listaPessoas.Any())
                return;
            
            var tripulantes = listaPessoas.Where(x => x.TipoPessoa.Equals(TipoPessoa.Policial) || x.TipoPessoa.Equals(TipoPessoa.Presidiario)).ToList();
            if (tripulantes.Count.Equals(QUANTIDADE_PRESENTE_LUGAR) && listaPessoas.Count > QUANTIDADE_PRESENTE_LUGAR)
                throw new ValidacaoException(MENSAGEM_ERRO_PRESIDIARIO_SOZINHO);
        }
    }
}