﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Base;

namespace CodeItAirlines.Validacao
{
    /*
    public class ValidacaoTipoTripulacao : IValidacaoTipoTripulacao
    {
        private const string MENSAGEM_ERRO_TRIPULACAO_TECNICA = "Para tripulação do tipo Tecnica é permitido somente pessoas do tipo Piloto ou Oficial.";
        private const string MENSAGEM_ERRO_TRIPULACAO_CABINE = "Para tripulação do tipo Cabine é permitido somente pessoas do tipo Chefe de Vôo ou Comissária.";
        private const string MENSAGEM_ERRO_TRIPULACAO_PASSAGEIRO = "Para tripulação do tipo Passageiro é permitido somente pessoas do tipo Policial ou Presidiario.";
        private const string MENSAGEM_ERRO_TRIPULACAO_INEXISTENTE = "Tipo de tripulação inválida.";

        private readonly IResultadoValidacao _validacoes;

        public ValidacaoTipoTripulacao(IResultadoValidacao validacoes)
        {
            _validacoes = validacoes;
        }

        public ValidacaoTipoTripulacao()
            : this(new ResultadoValidacao())
        { }

        public void Validar(IPessoa pessoa)
        {
            switch (pessoa.TipoTripulacao)
            {
                case TipoTripulacao.Tecnica:
                    ValidarTipoTripulacaoTecnica(pessoa.TipoPessoa);
                    break;
                case TipoTripulacao.Cabine:
                    ValidarTipoTripulacaoCabine(pessoa.TipoPessoa);
                    break;
                case TipoTripulacao.Passageiro:
                    ValidarTipoTripulacaoPassageiro(pessoa.TipoPessoa);
                    break;
                default:
                    _validacoes.Add(MENSAGEM_ERRO_TRIPULACAO_INEXISTENTE);
                    break;
            }
        }

        private void ValidarTipoTripulacaoTecnica(TipoPessoa tipoPessoa)
        {
            if (!TipoPessoa.Piloto.Equals(tipoPessoa) || !TipoPessoa.Oficial.Equals(tipoPessoa))
                _validacoes.Add(MENSAGEM_ERRO_TRIPULACAO_TECNICA);
        }

        private void ValidarTipoTripulacaoCabine(TipoPessoa tipoPessoa)
        {
            if (!TipoPessoa.ChefeServicoVoo.Equals(tipoPessoa) || !TipoPessoa.Comissaria.Equals(tipoPessoa))
                _validacoes.Add(MENSAGEM_ERRO_TRIPULACAO_CABINE);
        }

        private void ValidarTipoTripulacaoPassageiro(TipoPessoa tipoPessoa)
        {
            if (!TipoPessoa.Policial.Equals(tipoPessoa) || !TipoPessoa.Presidiario.Equals(tipoPessoa))
                _validacoes.Add(MENSAGEM_ERRO_TRIPULACAO_PASSAGEIRO);
        }
    }*/
}