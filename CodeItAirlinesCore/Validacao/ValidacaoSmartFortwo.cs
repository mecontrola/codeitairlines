﻿using CodeItAirlines.Exceptions;
using CodeItAirlines.Locais;
using CodeItAirlines.Validacao.Interface;

namespace CodeItAirlines.Validacao
{
    public class ValidacaoSmartFortwo : IValidacaoMotorista
    {
        private const string MENSAGEM_ERRO_PERMISSAO = "Somente o Piloto, Chefe de Serviço de Vôo e o Policial possuem permissão para dirigir.";

        public void Validar(SmartFortwo smartFortwo)
        {
            if (smartFortwo == null)
                return;

            if (smartFortwo.Motorista == null)
                throw new ValidacaoException(MENSAGEM_ERRO_PERMISSAO);
        }
    }
}