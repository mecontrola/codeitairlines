﻿using CodeItAirlines.Enum;
using CodeItAirlines.Exceptions;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao.Interface;
using System.Collections.Generic;
using System.Linq;

namespace CodeItAirlines.Validacao
{
    public class ValidacaoChefeServicoVooSozinhoComOficial : IValidacao
    {
        private const string MENSAGEM_ERRO_CHEFE_SERVICO_VOO_SOZINHO = "Não é permitido o chefe de serviço de vôo ficar sozinho com os oficiais.";

        public void Validar(ICollection<IPessoa> listaPessoas)
        {
            if (listaPessoas == null || !listaPessoas.Any())
                return;

            var chefeServicoVoo = listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.ChefeServicoVoo));
            if (!chefeServicoVoo)
                return;

            var oficial = listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.Oficial));
            if (oficial && listaPessoas.Count <= 3)
                throw new ValidacaoException(MENSAGEM_ERRO_CHEFE_SERVICO_VOO_SOZINHO);
        }
    }
}