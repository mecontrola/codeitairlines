﻿using CodeItAirlines.Enum;
using CodeItAirlines.Exceptions;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao.Interface;
using System.Collections.Generic;
using System.Linq;

namespace CodeItAirlines.Validacao
{
    public class ValidacaoPilotoSozinhoComComissaria : IValidacao
    {
        private const string MENSAGEM_ERRO_PILOTO_SOZINHO = "Não é permitido o piloto ficar sozinho com as comissárias.";

        public void Validar(ICollection<IPessoa> listaPessoas)
        {
            if (listaPessoas == null || !listaPessoas.Any())
                return;

            var piloto = listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.Piloto));
            if (!piloto)
                return;

            var comissaria = listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.Comissaria));
            if (comissaria && listaPessoas.Count <= 3)
                throw new ValidacaoException(MENSAGEM_ERRO_PILOTO_SOZINHO);
        }
    }
}