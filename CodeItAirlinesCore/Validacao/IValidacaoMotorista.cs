﻿using CodeItAirlines.Locais;

namespace CodeItAirlines.Validacao.Interface
{
    public interface IValidacaoMotorista
    {
        void Validar(SmartFortwo smartFortwo);
    }
}