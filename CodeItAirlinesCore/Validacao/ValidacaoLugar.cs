﻿using CodeItAirlines.Enum;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao.Interface;
using System.Collections.Generic;
using System.Linq;

namespace CodeItAirlines.Validacao
{
    public class ValidacaoLugar : IValidacao
    {
        private readonly IValidacao _validacaoChefeServicoVooSozinhoComOficial;
        private readonly IValidacao _validacaoPilotoSozinhoComComissaria;
        private readonly IValidacao _validacaoPresidiarioSemPolicial;

        public ValidacaoLugar(IValidacao validacaoChefeServicoVooSozinhoComOficial,
                              IValidacao validacaoPilotoSozinhoComComissaria,
                              IValidacao validacaoPresidiarioSemPolicial)
        {
            _validacaoChefeServicoVooSozinhoComOficial = validacaoChefeServicoVooSozinhoComOficial;
            _validacaoPilotoSozinhoComComissaria = validacaoPilotoSozinhoComComissaria;
            _validacaoPresidiarioSemPolicial = validacaoPresidiarioSemPolicial;
        }

        public ValidacaoLugar()
            : this(new ValidacaoChefeServicoVooSozinhoComOficial(),
                   new ValidacaoPilotoSozinhoComComissaria(),
                   new ValidacaoPresidiarioSemPolicial())
        { }

        public void Validar(ICollection<IPessoa> listaPessoas)
        {
            if (listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.ChefeServicoVoo) || x.TipoPessoa.Equals(TipoPessoa.Oficial)))
                _validacaoChefeServicoVooSozinhoComOficial.Validar(listaPessoas);

            if (listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.Piloto) || x.TipoPessoa.Equals(TipoPessoa.Comissaria)))
                _validacaoPilotoSozinhoComComissaria.Validar(listaPessoas);

            if (listaPessoas.Any(x => x.TipoPessoa.Equals(TipoPessoa.Policial) || x.TipoPessoa.Equals(TipoPessoa.Presidiario)))
                _validacaoPresidiarioSemPolicial.Validar(listaPessoas);
        }
    }
}