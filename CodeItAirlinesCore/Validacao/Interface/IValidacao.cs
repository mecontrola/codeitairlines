﻿using CodeItAirlines.Pessoas.Interface;
using System.Collections.Generic;

namespace CodeItAirlines.Validacao.Interface
{
    public interface IValidacao
    {
        void Validar(ICollection<IPessoa> listaPessoas);
    }
}