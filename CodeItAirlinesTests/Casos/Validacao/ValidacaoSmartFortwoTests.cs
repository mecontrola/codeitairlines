﻿using CodeItAirlines.Exceptions;
using CodeItAirlines.Locais;
using CodeItAirlines.Pessoas;
using CodeItAirlines.Validacao;
using CodeItAirlines.Validacao.Interface;
using CodeItAirlinesTests.Helper;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodeItAirlinesTests.Casos.Validacao
{
    [TestFixture]
    public class ValidacaoSmartFortwoTests
    {
        private const string MENSAGEM_ERRO_PERMISSAO = "Somente o Piloto, Chefe de Serviço de Vôo e o Policial possuem permissão para dirigir.";

        private IValidacaoMotorista _validacao;

        [SetUp]
        public void SetUp()
        {
            _validacao = new ValidacaoSmartFortwo();
        }

        private static IEnumerable<TestCaseData> TiposMotoristas
        {
            get
            {
                yield return new TestCaseData(
                        new SmartFortwo
                        {
                            Motorista = new Policial()
                        },
                        null
                    )
                    .SetName("[ValidacaoSmartFortwo] Deve não gerar exceção quando motorista for policial.");

                yield return new TestCaseData(
                        new SmartFortwo
                        {
                            Motorista = new ChefeServicoVoo()
                        },
                        null
                    )
                    .SetName("[ValidacaoSmartFortwo] Deve não gerar exceção quando motorista for chefe de serviçso de vôo.");

                yield return new TestCaseData(
                        new SmartFortwo
                        {
                            Motorista = new Piloto()
                        },
                        null
                    )
                    .SetName("[ValidacaoSmartFortwo] Deve não gerar exceção quando motorista for piloto.");
            }
        }


        [TestCaseSource(nameof(TiposMotoristas))]
        public void Deve_realizar_validacao_chefe_servico_voo_sozinho_com_oficial(SmartFortwo smartFortwo, string mensagem)
        {
            if (string.IsNullOrWhiteSpace(mensagem))
            {
                Assert.DoesNotThrow(() => { _validacao.Validar(smartFortwo); });
            }
            else
            {
                var excecao = AssertExtension.DoesNotThrows<ValidacaoException>(() => _validacao.Validar(smartFortwo));
                excecao.Message.Should().Be(mensagem);
            }
        }
    }
}