﻿using CodeItAirlines.Exceptions;
using CodeItAirlines.Pessoas;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using CodeItAirlines.Validacao.Interface;
using CodeItAirlinesTests.Helper;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodeItAirlinesTests.Casos.Validacao
{
    [TestFixture]
    public class ValidacaoChefeServicoVooSozinhoComOficialTests
    {

        private const string MENSAGEM_ERRO_CHEFE_SERVICO_VOO_SOZINHO = "Não é permitido o chefe de serviço de vôo ficar sozinho com os oficiais.";

        private IValidacao _validacao;

        [SetUp]
        public void SetUp()
        {
            _validacao = new ValidacaoChefeServicoVooSozinhoComOficial();
        }

        private static IEnumerable<TestCaseData> TiposPessoas
        {
            get
            {
                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Piloto(),
                            new Oficial(),
                            new Oficial(),
                            new ChefeServicoVoo(),
                            new Comissaria(),
                            new Comissaria(),
                            new Policial(),
                            new Presidiario()
                        },
                        null
                    )
                    .SetName("[ValidacaoChefeServicoVooSozinhoComOficial] Deve não gerar exceção quando a lista estiver completa.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Oficial(),
                            new Oficial(),
                            new ChefeServicoVoo(),
                            new Policial(),
                            new Presidiario()
                        },
                        null
                    ).SetName("[ValidacaoChefeServicoVooSozinhoComOficial] Deve não gerar erro caso existe o chefe de serviço de vôo e oficiais com mais alguma outra pessoa.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new ChefeServicoVoo(),
                            new Oficial(),
                            new Oficial()
                        },
                        MENSAGEM_ERRO_CHEFE_SERVICO_VOO_SOZINHO
                    ).SetName("[ValidacaoChefeServicoVooSozinhoComOficial] Deve gerar erro caso existe somente um chefe de serviço de vôo e dois oficiais.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new ChefeServicoVoo(),
                            new Oficial()
                        },
                        MENSAGEM_ERRO_CHEFE_SERVICO_VOO_SOZINHO
                    ).SetName("[ValidacaoChefeServicoVooSozinhoComOficial] Deve não gerar erro caso existe somente um chefe de serviço de vôo e um oficial.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Oficial(),
                            new Oficial()
                        },
                        null
                    ).SetName("[ValidacaoChefeServicoVooSozinhoComOficial] Deve não gerar erro caso exista somente oficiais.");

                yield return new TestCaseData(
                        new List<IPessoa>(),
                        null
                    ).SetName("[ValidacaoChefeServicoVooSozinhoComOficial] Deve não gerar erro quando a lista estiver vazia.");
            }
        }

        [TestCaseSource(nameof(TiposPessoas))]
        public void Deve_realizar_validacao_chefe_servico_voo_sozinho_com_oficial(ICollection<IPessoa> lista, string mensagem)
        {
            if (string.IsNullOrWhiteSpace(mensagem))
            {
                Assert.DoesNotThrow(() => { _validacao.Validar(lista); });
            }
            else
            {
                var excecao = AssertExtension.DoesNotThrows<ValidacaoException>(() => _validacao.Validar(lista));
                excecao.Message.Should().Be(mensagem);
            }
        }
    }
}

