using CodeItAirlines.Exceptions;
using CodeItAirlines.Pessoas;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using CodeItAirlines.Validacao.Interface;
using CodeItAirlinesTests.Helper;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodeItAirlinesTests.Casos.Validacao
{
    [TestFixture]
    public class ValidacaoPresidiarioSemPolicialTest
    {
        private const string MENSAGEM_ERRO_PRESIDIARIO_SOZINHO = "N�o � permitido o presidi�rio ficar sem a presen�a de um policial.";

        private IValidacao _validacao;

        [SetUp]
        public void SetUp()
        {
            _validacao = new ValidacaoPresidiarioSemPolicial();
        }

        private static IEnumerable<TestCaseData> TiposPessoas
        {
            get
            {
                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Piloto(),
                            new Oficial(),
                            new Oficial(),
                            new ChefeServicoVoo(),
                            new Comissaria(),
                            new Comissaria(),
                            new Policial(),
                            new Presidiario()
                        },
                        null
                    )
                    .SetName("[ValidacaoPresidiarioSemPolicial] Deve n�o gerar exce��o quando a lista estiver completa.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Oficial(),
                            new Comissaria(),
                            new Piloto(),
                            new Presidiario()
                        },
                        MENSAGEM_ERRO_PRESIDIARIO_SOZINHO
                    ).SetName("[ValidacaoPresidiarioSemPolicial] Deve gerar erro caso exista presidi�rio sem a presen�a do policial.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Policial(),
                            new Presidiario()
                        },
                        null
                    ).SetName("[ValidacaoPresidiarioSemPolicial] Deve n�o gerar erro caso existe somente um policial e presidi�rio.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Presidiario()
                        },
                        null
                    ).SetName("[ValidacaoPresidiarioSemPolicial] Deve n�o gerar erro caso exista somente presidi�rio.");

                yield return new TestCaseData(
                        new List<IPessoa>(),
                        null
                    ).SetName("[ValidacaoPresidiarioSemPolicial] Deve n�o gerar erro quando a lista estiver vazia.");
            }
        }

        [TestCaseSource(nameof(TiposPessoas))]
        public void Deve_realizar_validacao_chefe_servico_voo_sozinho_com_oficial(ICollection<IPessoa> lista, string mensagem)
        {
            if (string.IsNullOrWhiteSpace(mensagem))
            {
                Assert.DoesNotThrow(() => { _validacao.Validar(lista); });
            }
            else
            {
                var excecao = AssertExtension.DoesNotThrows<ValidacaoException>(() => _validacao.Validar(lista));
                excecao.Message.Should().Be(mensagem);
            }
        }
    }
}