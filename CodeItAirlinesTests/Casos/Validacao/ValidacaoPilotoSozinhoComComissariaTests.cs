﻿using CodeItAirlines.Exceptions;
using CodeItAirlines.Pessoas;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using CodeItAirlines.Validacao.Interface;
using CodeItAirlinesTests.Helper;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodeItAirlinesTests.Casos.Validacao
{
    [TestFixture]
    public class ValidacaoPilotoSozinhoComComissariaTests
    {
        private const string MENSAGEM_ERRO_PILOTO_SOZINHO = "Não é permitido o piloto ficar sozinho com as comissárias.";

        private IValidacao _validacao;

        [SetUp]
        public void SetUp()
        {
            _validacao = new ValidacaoPilotoSozinhoComComissaria();
        }

        private static IEnumerable<TestCaseData> TiposPessoas
        {
            get
            {
                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Piloto(),
                            new Oficial(),
                            new Oficial(),
                            new ChefeServicoVoo(),
                            new Comissaria(),
                            new Comissaria(),
                            new Policial(),
                            new Presidiario()
                        },
                        null
                    )
                    .SetName("[ValidacaoPilotoSozinhoComComissaria] Deve não gerar exceção quando a lista estiver completa.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Comissaria(),
                            new Comissaria(),
                            new Piloto(),
                            new Policial(),
                            new Presidiario()
                        },
                        null
                    ).SetName("[ValidacaoPilotoSozinhoComComissaria] Deve não gerar erro caso existe o chefe de serviço de vôo e oficiais com mais alguma outra pessoa.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Piloto(),
                            new Comissaria(),
                            new Comissaria()
                        },
                        MENSAGEM_ERRO_PILOTO_SOZINHO
                    ).SetName("[ValidacaoPilotoSozinhoComComissaria] Deve gerar erro caso existe somente um piloto e duas comissarias.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Piloto(),
                            new Comissaria()
                        },
                        MENSAGEM_ERRO_PILOTO_SOZINHO
                    ).SetName("[ValidacaoPilotoSozinhoComComissaria] Deve não gerar erro caso existe somente um piloto e uma comissaria.");

                yield return new TestCaseData(
                        new List<IPessoa>
                        {
                            new Comissaria(),
                            new Comissaria()
                        },
                        null
                    ).SetName("[ValidacaoPilotoSozinhoComComissaria] Deve não gerar erro caso exista somente comissarias.");

                yield return new TestCaseData(
                        new List<IPessoa>(),
                        null
                    ).SetName("[ValidacaoPilotoSozinhoComComissaria] Deve não gerar erro quando a lista estiver vazia.");
            }
        }

        [TestCaseSource(nameof(TiposPessoas))]
        public void Deve_realizar_validacao_chefe_servico_voo_sozinho_com_oficial(ICollection<IPessoa> lista, string mensagem)
        {
            var excecao = AssertExtension.DoesNotThrows<ValidacaoException>(() => _validacao.Validar(lista));
            if (string.IsNullOrWhiteSpace(mensagem))
                excecao.Should().BeNull();
            else
                excecao.Message.Should().Be(mensagem);
        }
    }
}