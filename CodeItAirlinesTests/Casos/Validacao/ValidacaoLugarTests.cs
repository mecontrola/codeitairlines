﻿using CodeItAirlines.Pessoas;
using CodeItAirlines.Pessoas.Interface;
using CodeItAirlines.Validacao;
using CodeItAirlines.Validacao.Interface;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeItAirlinesTests.Casos.Validacao
{
    [TestFixture]
    public class ValidacaoLugarTests
    {
        private IValidacao _validacaoChefeServicoVooSozinhoComOficial;
        private IValidacao _validacaoPilotoSozinhoComComissaria;
        private IValidacao _validacaoPresidiarioSemPolicial;

        [SetUp]
        public void SetUp()
        {
            _validacaoChefeServicoVooSozinhoComOficial = Substitute.For<IValidacao>();
            _validacaoPilotoSozinhoComComissaria = Substitute.For<IValidacao>();
            _validacaoPresidiarioSemPolicial = Substitute.For<IValidacao>();
        }

        private ValidacaoLugar InicializarObjeto()
        {
            return new ValidacaoLugar(_validacaoChefeServicoVooSozinhoComOficial,
                                      _validacaoPilotoSozinhoComComissaria,
                                      _validacaoPresidiarioSemPolicial);
        }

        [TestCase(TestName = "[ValidacaoLugar] Deve chamar a validação ValidacaoChefeServicoVooSozinhoComOficial.")]
        public void Deve_validar_chefe_servico_voo_sozinho_com_oficial()
        {
            var lista = new List<IPessoa>
            {
                new Oficial()
            };

            InicializarObjeto().Validar(lista);

            _validacaoChefeServicoVooSozinhoComOficial.Received(1);
            _validacaoPilotoSozinhoComComissaria.Received(0);
            _validacaoPresidiarioSemPolicial.Received(0);
        }

        [TestCase(TestName = "[ValidacaoLugar] Deve chamar a validação ValidacaoPilotoSozinhoComComissaria.")]
        public void Deve_validar_Piloto_Sozinho_Com_Comissaria()
        {
            var lista = new List<IPessoa>
            {
                new Piloto()
            };

            InicializarObjeto().Validar(lista);

            _validacaoChefeServicoVooSozinhoComOficial.Received(0);
            _validacaoPilotoSozinhoComComissaria.Received(1);
            _validacaoPresidiarioSemPolicial.Received(0);
        }

        [TestCase(TestName = "[ValidacaoLugar] Deve chamar a validação ValidacaoPresidiarioSemPolicial.")]
        public void Deve_validar_Presidiario_Sem_Policial()
        {
            var lista = new List<IPessoa>
            {
                new Presidiario()
            };

            InicializarObjeto().Validar(lista);

            _validacaoChefeServicoVooSozinhoComOficial.Received(0);
            _validacaoPilotoSozinhoComComissaria.Received(0);
            _validacaoPresidiarioSemPolicial.Received(1);
        }


        [TestCase(TestName = "[ValidacaoLugar] Deve chamar nenhuma validação quando a lista estiver vazia.")]
        public void Deve_validar_lista_vazia()
        {
            var lista = new List<IPessoa>();

            InicializarObjeto().Validar(lista);

            _validacaoChefeServicoVooSozinhoComOficial.Received(0);
            _validacaoPilotoSozinhoComComissaria.Received(0);
            _validacaoPresidiarioSemPolicial.Received(0);
        }
    }
}