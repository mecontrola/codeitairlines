﻿using System;

namespace CodeItAirlinesTests.Helper
{
    public static class AssertExtension
    {
        public static T DoesNotThrows<T>(Action expressionUnderTest) where T : Exception
        {
            try
            {
                expressionUnderTest();
            }
            catch (T exception)
            {
                return exception;
            }

            return null;
        }

    }
}